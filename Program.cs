﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TableGraph
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 4) {
                return;
            }

            var dbServer = args[0];
            var catalog = args[1];
            var user = args[2];
            var userPass = args[3];

            var tables = Table.GetTables(dbServer, catalog, user, userPass)
                .ToList()
                .OrderBy(t => t.Value.DependencyDepth);
            foreach (var t in tables)
            {
                var value = t.Value;
                var s = value.ToString();
                Console.WriteLine(s);
            }
        }
    }

    public class Table
    {
        public string Name { get; set; }
        public HashSet<Table> Dependencies { get; set; }

        public Table()
        {
            this.Dependencies = new HashSet<Table>();
        }

        public int DependencyDepth
        {
            get {
                return this.DependencyDepthFn(0);
            }
        }

        private int DependencyDepthFn(int depth)
        {
            if (this.Dependencies.Count() < 1) {
                return depth;
            }
            return this.Dependencies.Select(d => d.DependencyDepthFn(depth+1)).Max();
        }


        public static string Prefix(int depth)
        {
            if (depth < 1) {
                return "";
            }
            var builder = new StringBuilder();
            for (var i = 0; i < depth; i++) {
                builder.Append("    ");
            }

            return builder.ToString();
        }

        public override string ToString()
        {
            var treeList = new List<string>();
            var s = this.Treeize(0, treeList)
                .Aggregate((a,b) => $"{a}\n{b}");
            return s;
        }

        public List<string> Treeize(int depth, List<string> tableTree)
        {
            tableTree.Add($"{Prefix(depth)}{this.Name}");

            foreach (var dep in this.Dependencies) {
                tableTree = dep.Treeize(depth+1, tableTree);
            }
            return tableTree;
        }

        public static Dictionary<string, Table> GetTables(string server, string intialCatalog, string user, string pass)
        {
            var tablesQuery = @"
            SELECT TABLE_CATALOG,
                TABLE_SCHEMA,
                TABLE_NAME
            FROM 
                INFORMATION_SCHEMA.TABLES
            ORDER BY TABLE_NAME
            ";

            var depsQuery = @"
            SELECT fk.TABLE_CATALOG,
                fk.TABLE_SCHEMA,
                fk.TABLE_NAME,
                pk.TABLE_CATALOG,
                pk.TABLE_SCHEMA,
                pk.TABLE_NAME
            FROM 
                INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
                JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS fk
                    ON (rc.CONSTRAINT_NAME = fk.CONSTRAINT_NAME)
                JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk
                    ON rc.UNIQUE_CONSTRAINT_NAME = pk.CONSTRAINT_NAME
            ORDER BY fk.TABLE_NAME
            ";

            var tables = new Dictionary<string, Table>();

            var connectionBuilder = new SqlConnectionStringBuilder();
            connectionBuilder.DataSource = server;
            connectionBuilder.InitialCatalog = intialCatalog;
            connectionBuilder.IntegratedSecurity = false;
            connectionBuilder.UserID = user;
            connectionBuilder.Password = pass;

            using (var connection = new SqlConnection(connectionBuilder.ConnectionString)) {
                connection.Open();
                var tablesCommand = connection.CreateCommand();
                tablesCommand.CommandType = CommandType.Text;
                tablesCommand.CommandText = tablesQuery;

                var reader = tablesCommand.ExecuteReader();
                if (reader.HasRows) {
                    string catalog;
                    string schema;
                    string name;
                    while (reader.Read()) {
                        catalog = reader.GetString(0);
                        schema = reader.GetString(1);
                        name = reader.GetString(2);

                        var tableName = $"{catalog}.{schema}.{name}";
                        if (!tables.ContainsKey(tableName)) {
                            tables.Add(tableName, new Table { Name = tableName });
                        }
                    }
                }
                reader.Close();

                var dependenciesCommand = connection.CreateCommand();
                dependenciesCommand.CommandType = CommandType.Text;
                dependenciesCommand.CommandText = depsQuery;
                reader = dependenciesCommand.ExecuteReader();

                if (reader.HasRows) {
                    string referencedCatalog;
                    string referencedSchema;
                    string referencedName;

                    string catalog;
                    string schema;
                    string name;

                    while (reader.Read()) {
                        catalog = reader.GetString(0);
                        schema = reader.GetString(1);
                        name = reader.GetString(2);

                        referencedCatalog = reader.GetString(3);
                        referencedSchema = reader.GetString(4);
                        referencedName = reader.GetString(5);

                        var tableName = $"{catalog}.{schema}.{name}";
                        var referencedTableName = $"{referencedCatalog}.{referencedSchema}.{referencedName}";

                        // Avoid the devil
                        if (tableName.Equals(referencedTableName)) {
                            continue;
                        }

                        if (tables.ContainsKey(tableName) && tables.ContainsKey(referencedTableName)) {
                            var table = tables.GetValueOrDefault(tableName);
                            var referencedTable = tables.GetValueOrDefault(referencedTableName);
                            table.Dependencies.Add(referencedTable);
                        }
                    }
                }

                reader.Close();
            }

            return tables;
        }
    }
}
